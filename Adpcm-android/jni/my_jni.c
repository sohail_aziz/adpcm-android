/**
 * 
 * @author Sohail Aziz
 * sohailaziz05.blogspot.com
 * sohail.aziz05@gmail.com
 *
 */

#include "my_jni.h"
#include <android/log.h>
#include "my_main.h"

/**
 * this method should not be called on UI thread
 */
JNIEXPORT void JNICALL Java_sohail_aziz_adpcm_MainActivity_encodeToAdpcm
(JNIEnv *env, jobject javaThis, jstring inputPath, jstring outputPath) {

	__android_log_print(ANDROID_LOG_DEBUG, "sohail",
			"JNI:=======ENCODE TO ADPCM=======");

	jboolean c = JNI_TRUE;
	const char* inPath = (*env)->GetStringUTFChars(env, inputPath, &c);
	const char* outPath = (*env)->GetStringUTFChars(env, outputPath, &c);

	__android_log_print(ANDROID_LOG_DEBUG, "sohail",
			"JNI:=======EncodeToADPCM=======");
	__android_log_print(ANDROID_LOG_DEBUG, "sohail",
			"JNI:EncodeToAdpcm  inPath %s", inPath);
	__android_log_print(ANDROID_LOG_DEBUG, "sohail",
			"JNI:EncodeToAdpcm  outPath %s", outPath);

	// doEncode(inPath, outPath);
	//void doEncodeSend(const char* inputPath, const char* outputPath)
	doEncodeSend(inPath,outPath);

	(*env)->ReleaseStringUTFChars(env,inputPath,inPath);
	(*env)->ReleaseStringUTFChars(env, outputPath, outPath);

}

/**
 * this method should not be called on UI thread
 */
JNIEXPORT void JNICALL Java_sohail_aziz_adpcm_MainActivity_decodeToPcm
(JNIEnv *env, jobject javaThis, jstring inputPath, jstring outputPath) {

	__android_log_print(ANDROID_LOG_DEBUG, "sohail",
			"JNI:=======DECODE TO PCM=======");

	jboolean c = JNI_TRUE;
	const char* inPath = (*env)->GetStringUTFChars(env, inputPath, &c);
	const char* outPath = (*env)->GetStringUTFChars(env, outputPath, &c);

	__android_log_print(ANDROID_LOG_DEBUG, "sohail",
			"JNI:=======DECODE TO PCM=======");
	__android_log_print(ANDROID_LOG_DEBUG, "sohail",
			"JNI:DECODE TO PCM  inPath %s", inPath);
	__android_log_print(ANDROID_LOG_DEBUG, "sohail",
			"JNI:DECODE TO PCM  outPath %s", outPath);

	doDecodeReceive(inPath, outPath);
	// doDecode(inPath,outPath);

	(*env)->ReleaseStringUTFChars(env,inputPath,inPath);
	(*env)->ReleaseStringUTFChars(env, outputPath, outPath);

}
