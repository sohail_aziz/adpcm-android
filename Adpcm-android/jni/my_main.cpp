/**
 * 
 * @author Sohail Aziz
 * sohailaziz05.blogspot.com
 * sohail.aziz05@gmail.com
 *
 */
//#include <fstream>
#include <iostream>
#include <fstream>
#include <android/log.h>
#include "ima_adpcm.h"
#include "my_main.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

using namespace std;


void getShorts(int16_t* shortBuf, int shortLen, char* buf) {

	int nextIndex = 0;
	for (int i = 0; i < shortLen; i++) {

		shortBuf[i] = combine(buf[nextIndex], buf[nextIndex + 1]);
		nextIndex += 2;

	}

}

int16_t combine(uint8_t byteOne, uint8_t byteTwo) {
	return byteTwo << 8 | byteOne;

}



void doEncodeSend(const char* inputPath, const char* outputPath) {

	__android_log_print(ANDROID_LOG_DEBUG, "sohail",
			"JNI:======= doEncode started=======");



	IMA_ADPCM Adpcm = IMA_ADPCM();

	ifstream inputFile(inputPath, ios::binary | ios::in);
	ofstream outFile(outputPath, ios::binary | ios::out);

	int len = 0;
	if (inputFile != NULL) {

		inputFile.seekg(0, inputFile.end);
		len = inputFile.tellg();

		//move to first position
		inputFile.seekg(0, inputFile.beg);
	} else {
		__android_log_print(ANDROID_LOG_DEBUG, "sohail",
				"JNI:======= doEncode: inputFile is NULL");
	}

	int INPUT_SIZE = 4096;
	long index = 0;
	int SIZE_INDEX = 11;
	char cIndex[SIZE_INDEX];
	char *buf = new char[INPUT_SIZE];
	char *sendBuf = new char[1024 + SIZE_INDEX]; //first 4 bytes represents chunk no
	int sendBufCount = 0;

	int16_t* inputBuf = new int16_t[INPUT_SIZE / 2];

	uint8_t *outbuf = new uint8_t[INPUT_SIZE / 4];
	long offset = 0; //no of samples

	//encode init
	char* init_buf = new char[4]; //2 samples of 16bit
	inputFile.read(init_buf, 4);
	int16_t sample1 = combine(init_buf[0], init_buf[1]);
	int16_t sample2 = combine(init_buf[2], init_buf[3]);
	Adpcm.EncodeInit(sample1, sample2);

	////encoding.....
	int packetNo = 0;
	while (index < len) {

		inputFile.read(buf, INPUT_SIZE * sizeof(char));

		if (inputFile.gcount() < INPUT_SIZE) {
			break;
		}

		getShorts(inputBuf, INPUT_SIZE / 2, buf);

		int n = Adpcm.Encode((uint8_t*) outbuf, offset, inputBuf, INPUT_SIZE);

		if (outbuf != NULL) {
			outFile.write((char*) outbuf, INPUT_SIZE / 4);

			index += INPUT_SIZE;

		} else {
			break;
		}

	}

	inputFile.close();

	outFile.flush();
	outFile.close();

	__android_log_print(ANDROID_LOG_DEBUG, "sohail",
			"JNI:=======DONE doEncode =======");

}

void doDecodeReceive(const char* inputPath, const char* outputPath) {

	__android_log_print(ANDROID_LOG_DEBUG, "sohail",
			"JNI:======= doDecode started =======");


	IMA_ADPCM Adpcm = IMA_ADPCM();
	int INPUT_SIZE = 1024 / 4;

	ifstream inputFile(inputPath, ios::binary | ios::in);
	ofstream outFile(outputPath, ios::binary | ios::out);

	int len = 0;
	if (inputFile != NULL) {

		inputFile.seekg(0, inputFile.end);
		len = inputFile.tellg();

		//move to first position
		inputFile.seekg(0, inputFile.beg);
	} else {
		__android_log_print(ANDROID_LOG_DEBUG, "sohail",
				"JNI:======= doDecode: inputFile is NULL");
	}


	int index = 0;
	char *buf = new char[INPUT_SIZE];
	int16_t *outbuf = new int16_t[INPUT_SIZE * 2];
	int offset = 0;
	while (index < len) {

		//read two bytes

		inputFile.read(buf, INPUT_SIZE * sizeof(char));

		//give them to decode
		int n = Adpcm.Decode(outbuf, (uint8_t*) buf, offset, INPUT_SIZE * 8);

		if (outbuf != NULL) {

			outFile.write((char*) outbuf, INPUT_SIZE * 4 * sizeof(char));
			index += INPUT_SIZE;
		} else {
			break;
		}

	}

	outFile.flush();
	outFile.close();

	inputFile.close();

	__android_log_print(ANDROID_LOG_DEBUG, "sohail",
			"JNI:======= doDecode DONE =======");

}

