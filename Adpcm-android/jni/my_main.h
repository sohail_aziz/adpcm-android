#pragma once

#ifndef MY_MAIN_H
#define MY_MAIN_H

#include "common.h"

int16_t combine(uint8_t byteOne, uint8_t byteTwo);

#ifdef __cplusplus
extern "C" {
#endif
void startReceiving(const char* outputPath);
void doEncode(const char* inputPath, const char* outputPath);
void doDecode(const char* inputPath, const char* outputPath);
void doEncodeSend(const char* inputPath, const char* outputPath);
void doDecodeReceive(const char* inputPath, const char* outputPath);
#ifdef __cplusplus
}
#endif

#endif
