package sohail.aziz.adpcm;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.WindowManager;

public class MainActivity extends Activity {

	static {
		System.loadLibrary("myadpcm");
	}

	private native void encodeToAdpcm(String inputPath, String outputPath);

	private native void decodeToPcm(String inputPath, String outputPath);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


		findViewById(R.id.btEncode).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {

						doEncode();

					}
				});

		findViewById(R.id.btDecode).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						doDecode();

					}
				});

	}


	private void doEncode() {

		new Thread(new Runnable() {
			
			@Override
			public void run() {
				String inputPath = Environment.getExternalStorageDirectory()
						.getAbsolutePath() + "/" + "songwav.pcm";
				String outputPath = Environment.getExternalStorageDirectory()
						.getAbsolutePath() + "/" + "songwav.adpcm";
			
				encodeToAdpcm(inputPath, outputPath);

			}
		}).start();
		
	}

	private void doDecode() {
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				String inputPath = Environment.getExternalStorageDirectory()
						.getAbsolutePath() + "/" + "songwav.adpcm";

				String outputPath = Environment.getExternalStorageDirectory()
						.getAbsolutePath() + "/" + "decoded_songwav.pcm";

				decodeToPcm(inputPath, outputPath);
				
			}
		}).start();

	}

}
