### IMA-ADPCM encoder/decoder android ###

* IMA-ADPCM gives 4 times audio compression with little degradation in quality.
* This is a proof of concept for ima-adpcm encoding/decoding using JNI in android.
* Version 1.0


### How to compile? ###

* Move to your main project directory in shell (linux or cygwin)
* Run 
  **$NDK_PATH/ndk-build** 
  where $NDK_PATH is your complete android ndk path.  
* Put 44100 hz, 16 bit stereo pcm file (without wav headers)in your sdcard, change the file names in Activity accordingly.
* Run the app and enjoy!